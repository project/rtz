<?php

namespace Drupal\rtz\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\DecimalFormatter;

/**
 * Plugin implementation of 'remove_trailing_zeros_number_decimal' formatter.
 *
 * Formatter that remove trailing zeros in decimal and float fields.
 * For instance, the number 7.000 wil be output as: 7
 *
 * @FieldFormatter(
 *   id = "remove_trailing_zeros_number_decimal",
 *   label = @Translation("Remove Trailing Zeros"),
 *   field_types = {
 *     "decimal",
 *     "float"
 *   }
 * )
 */
class RTZDecimalFormatter extends DecimalFormatter {

  /**
   * {@inheritdoc}
   */
  protected function numberFormat($number) {
    $output = number_format($number, $this->getSetting('scale'), $this->getSetting('decimal_separator'), $this->getSetting('thousand_separator'));
    // Remove trailing zeros.
    $output = rtrim($output, '0');
    $output = rtrim($output, $this->getSetting('decimal_separator'));

    return $output;
  }

}
