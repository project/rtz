----------------------------------------------------------------------------
                     D R U P A L    M O D U L E
----------------------------------------------------------------------------
Name: Remove Trailing Zeros
Author: Andrey Vitushkin <andrey.vitushkin at gmail.com>
Drupal: 9.000000000000000000000000000000000000000000000000000000000000000000


INTRODUCTION
------------
Provides formatter that remove trailing zeros in decimal and float fields.
For instance, the number 7.000 wil be output as: 7


REQUIREMENTS
------------
This module does not requires additional modules.


INSTALLATION
------------
Install as you would normally install a contributed Drupal module.
For further information visit:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules


CONFIGURATION
-------------
The module has no menu or modifiable settings.
There is no configuration.


HOW TO USE
------------
1. Visit 'Manage display' tab of your content type.
2. Select 'Remove Trailing Zeros' format for your decimal or float field.
3. Click on the 'Save' button.

If you want to remove trailing zeros in a view,
then select 'Remove Trailing Zeros' formatter in the fields settings.
